//
//  CustomTableViewCell.swift
//  Lesson_9
//
//  Created by Vladdoriy on 07.12.2022.
//

import UIKit

protocol CustomCell {
    
    static func cellNib() -> UINib?
    
    static func cellIdentifire() -> String
    
}
protocol CustomCellDelegate: AnyObject {
    func didPressAction(for cell: UITableViewCell)
}

class CustomTableViewCell: UITableViewCell, CustomCell {

    
    @IBOutlet weak var LabelOne: UILabel!
    @IBOutlet weak var LabelTwo: UILabel!
    
    weak var delegate: CustomCellDelegate?
   
    func configure(with user: UserModel, delegate: CustomCellDelegate) {
        
        LabelOne.text = user.age
        LabelTwo.text = user.title
        
        self.delegate = delegate
    }
    static func cellNib() -> UINib?{
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    static func cellIdentifire() -> String{
        return "reuseIdentifier"
    }
   
    @IBAction func butttonAction(_ sender: Any) {
        delegate?.didPressAction(for: self)
    }
}



