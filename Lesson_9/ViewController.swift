//
//  ViewController.swift
//  Lesson_9
//
//  Created by Vladdoriy on 07.12.2022.
//

import UIKit

protocol ViewControllerDelegate: AnyObject {
    func didChangeUser(_ user: UserModel)
}

enum Names: String {
    case kostya = "Kostya"
    case vlad = "Vlad"
    case ilya = "Ilya"
    case klim = "Klim"
    case danik = "Danik"
    case lev = "Lev"
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CustomCellDelegate, ViewControllerDelegate {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var users: [UserModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        users.append(UserModel(title: "Kostya", age: "19"))
        users.append(UserModel(title: "Vlad", age: "20"))
        users.append(UserModel(title: "Ilya", age: "21"))
        users.append(UserModel(title: "Klim", age: "54"))
        users.append(UserModel(title: "Danik", age: "18"))
        users.append(UserModel(title: "Lev", age: "19"))
        
        
        
        tableView.delegate = self
        tableView.dataSource = self
       
        
        tableView.registerCustomCell(CustomTableViewCell.self)
        
        let firstUser = users.first!
        
        }
    
    func didPressAction(for cell: UITableViewCell) {
        
        if let indexPath = tableView.indexPath(for: cell){
            
            let model = users[indexPath.row]
            
            performSegue(withIdentifier: "detailIdentifier", sender: model)
        }
    }
    
    func didChangeUser(_ user: UserModel) {
        
        if let userValue = users.enumerated().first(where: { $0.element.title == user.title}) {
            
            
            users.remove(at: userValue.offset)
            users.insert(user, at: userValue.offset)
            
            tableView.reloadData()
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return users.count
            
        }
        
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomTableViewCell.cellIdentifire(), for: indexPath) as! CustomTableViewCell
        
            
            let model = users[indexPath.row]
            
        cell.configure(with: model, delegate: self)
           
            
            return cell
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
//        tableView.deselectRow(at: indexPath, animated: true)
//
//        let model = users[indexPath.row]
//
//        performSegue(withIdentifier: "detailIdentifier", sender: model)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailIdentifier", let UserModel = sender as? UserModel {
            
            let destControler = segue.destination as! DetailViewController
            destControler.user = UserModel
            destControler.delegate = self
            }
        }
    }
    

