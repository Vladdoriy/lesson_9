//
//  DetailViewController.swift
//  Lesson_9
//
//  Created by Vladdoriy on 08.12.2022.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var ageLabel: UILabel!
    
    var user: UserModel!
    weak var delegate: ViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = user.title
        ageLabel.text = user.age
        
        nameLabel.sizeToFit()
        ageLabel.sizeToFit()
        
        changeData()
    }
    
    private func changeData() {
        
        guard let ageint = Int(user.age) else {return}
        
        user.age = String(ageint + 22)
        
        delegate?.didChangeUser(user)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
