//
//  TableViewExtension.swift
//  Lesson_9
//
//  Created by Vladdoriy on 08.12.2022.
//

import Foundation
import UIKit

extension UITableView {
    
    func registerCustomCell(_ cell: CustomCell.Type) {
        
        self.register(cell.cellNib(), forCellReuseIdentifier: cell.cellIdentifire())
    }
    
}
